/*
 * As is
 */
package snipets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ProsteOperacjeNaPlikuTextowym {
    
    /**
     * Pozwala wczytać tekst z pliku do listy
     * @param nazwaPliku
     * @return lista zawierajaca wszystkie linie tekstu
     */
    public List<String> czytajPlikTextowy(String nazwaPliku){
        try {
            //udało się :)
            return Files.readAllLines(Paths.get(nazwaPliku));
        } catch (IOException ex) {
            Logger.getLogger(ProsteOperacjeNaPlikuTextowym.class.getName()).log(Level.SEVERE, null, ex);
        }
        // nie udalo sie wiec nic nie zwracamy :(
        return null;
    }
    
    /**
     * split - proste a poteżne
     */
    public void splitter(){
        String text ="a,b,c,d";
        String delim =",";
        // {"a","b","c","d"}
        String[] wynik = text.split(delim);
    }
    
    /**
     * Jenoliniowy zapis tekstu do pliku
     * @param nazwaPliku
     * @param zawartosc 
     */
    public void najprostszy(String nazwaPliku, String zawartosc){
        try {
            Files.write(Paths.get(nazwaPliku), zawartosc.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(ProsteOperacjeNaPlikuTextowym.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Zapis listy Stringów do pliku
     * @param nazwaPliku
     * @param lista 
     */
    public void zapisListyStringow(String nazwaPliku, List<String> lista){
        try {
            // metoda ma jeszcze paramtry pozwalajace sterować jak otwierac/ napisywac plik
            // szukaj w JavaDoc
            //public static Path write(Path path, Iterable<? extends CharSequence> lines, OpenOption... options) throws IOException
            Files.write(Paths.get(nazwaPliku), lista);
        } catch (IOException ex) {
            Logger.getLogger(ProsteOperacjeNaPlikuTextowym.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
