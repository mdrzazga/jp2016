package zajecia10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 */
public class Main {

    public static void main(String args[]) {
        List<Uczen> listaUczniow = new ArrayList<>();
        String[] imiona = "Adam Ewa Franek Sonia Kacper Olga".split(" ");
        String[] nazwiska = "Nowak Kowalik Jonsos Kloss Zanussi Bosh".split(" ");

        for (int i = 0; i < 15; i++) {
            listaUczniow.add(new Uczen(imiona[i % 6], nazwiska[i % 5], i));
        }
        // Wypiszemy liste
        System.out.println("Przed sortowaniem :");
        for (Uczen u : listaUczniow) {
            System.out.println(u);
        }
        // Kolejna niespodzianka - Anonimowa klasa wewnętrzna
        Collections.sort(listaUczniow, new Comparator<Uczen>() {
            @Override
            public int compare(Uczen o1, Uczen o2) {
                // Tu pownna zanleźć się odpowiedź na zagadkę
                int c = o1.getNazwisko().compareTo(o2.getNazwisko());
                if (c == 0) {
                    c = o1.getImie().compareTo(o2.getImie());
                }
                //Zagadka a co się stanie jak o1 albo o2 bedzie miało wartosc null ?
                return c;
            }
        });
        System.out.println();
        System.out.println("\nPo sortowaniu:");
        //Taka niespodzianka dla ciekawych - Java 8 Strumienie
        listaUczniow.stream().forEach(System.out::println);
        // mały eksperyment
        listaUczniow.addAll(listaUczniow);
        // wyswitlenie
        System.out.println("\nWynik eksperymentu:");
        // Java 8 w natarciu
        listaUczniow.forEach(System.out::println);

        // Set (zbiór to tez kolekcja)
        Set<Uczen> zbiorUczniow = new HashSet<>(listaUczniow);
        System.out.println("\nZawarosc zbioru: ");
        zbiorUczniow.forEach(System.out::println);

        // usuwamy 5 p[ierwszych uczniów z listy
        zbiorUczniow.removeAll(listaUczniow.subList(0, 5));
        System.out.println("\nZawartośc po modyfikacji");
        //Niespodzianka dla ciekawych - Java 8 Wyrażenie lambda
        zbiorUczniow.forEach(u -> System.out.println(u));

        //Czy na liści i w zbiorze mamy rózne obiekty ?
        //Eksperyment zmienimy nazwko ostatniemu na liście
        listaUczniow.get(listaUczniow.size() - 1).setNazwisko("XXXXXX");
        System.out.println("\n Zbiór po eksperymencie:");
        zbiorUczniow.forEach(System.out::println);
    }
}
