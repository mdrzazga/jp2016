package zajecia10;

/**
 * 
 */
public class Uczen {
   private String imie, nazwisko;
   private int nrIndeksu;

    public Uczen(String imie, String nazwisko, int nrIndeksu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrIndeksu = nrIndeksu;
    }

    //Pozwoli łatwo wyświetlić objekt w postaci tekstu
    @Override
    public String toString() {
        return "Uczen{" + "imie=" + imie + ", nazwisko=" + nazwisko + ", nrIndeksu=" + nrIndeksu + '}';
    }
    

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(int nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }
   
}
