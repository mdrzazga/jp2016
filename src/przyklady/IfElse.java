package przyklady;

public class IfElse {

    public static void main(String args[]) {
        boolean wymagane = true;
        if (args.length > 0) {
            System.out.println("Mamy argumenty");
        } else {
            System.out.println("Brak argumentów");
            // zagnieżdżone if
            if (wymagane) {
                System.out.println("Wymagamy argumentów !!!");
            }
        }

    }
}
