package przyklady;

public class ForAndForEach {

    public static void main(String args[]) {
        char[] tablica = {'A', 'B', 'C'};
        
        // klasyczny for 
        for (int i = 0; i < 3; i++) {
            System.out.print(tablica[i] + "\t");
        }
        
        System.out.println();
        
        // for each 
        for (char c : tablica) {
            System.out.print(c + "\t");
        }
        System.out.println();
    }
}
