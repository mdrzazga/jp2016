package przyklady;


public class SwitchCase {
    public static void main(String args[]){
       if(args.length > 0)test(args[0]);
       test(args.length);
    }

    public static void test(String arg) {
        switch(arg){
            case "A":
                System.out.println("Pierwsza litera alfabetu");
                break;
            case "Z":
                System.out.println("Ostatnia litera alfabetu");
                break;
            default:
                System.out.println("Trudno powiedzieć");
        }
    }
    
     public static void test(int arg) {
        switch(arg){
            case 0:
                System.out.println("zero");
                break;
            case 1:
                System.out.println("jeden");
                break;
            case 2:
                System.out.println("dwa");
                break;
            default:
                System.out.println("Trudno powiedzieć");
        }
    }
    
}
