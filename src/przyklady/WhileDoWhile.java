package przyklady;

public class WhileDoWhile {

    public static void main(String args[]) {
        int i, max;
        i=0;
        max =10;
        while( i < max ){
            System.out.print(i);
            i++;
        }
        System.out.println();
        
        i=0;
        do{
            System.out.print(i);
            i++;
        }while(i < max);
        System.out.println();
        
        i=0;
        max =0;
        while( i < max ){
            System.out.print(i);
            i++;
        }
        System.out.println();
        
        i=0;
        max=0;
        do{
            System.out.print(i);
            i++;
        }while(i < max);
        System.out.println();
    }
}
