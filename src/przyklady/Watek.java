package przyklady;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Wątki pozwalją wykonywać części programu niezależnie od siebie, 
 * na komputrach wyposażonych w wiele procesorów watki mogą być wykonywane równolegle.
 * Ostateczenie za szeregowanei i zrównoleglenie odpowiada system operacyjny. 
 * Wątki daja programiście uzyteczny model programownia wspóbieżnego.
 * Klasy które dziedziczą po java.lang.Thread mogą być uruchamiane jako osobne wątki.
 */
public class Watek extends Thread{
    String nazwa;
    long czasZycia;

    public Watek(String nazwa, long czasZycia) {
        this.nazwa = nazwa;
        this.czasZycia = czasZycia;
    }
    
    public static void main(String args[]){
        Watek w1 = new Watek("Pierwszy", 2000);
        w1.start();
        Watek w2 = new Watek("Drugi", 1000);
        w2.start();
        // Drugim częściej spotykanym sposobem na wielowątkowość 
        // jest implementacja interfejsy Runnable
        // Króey zaimplementujemy w klasie anonimowej
        Runnable runIt =  new Runnable() {

            @Override
            public void run() {
                try {
                    System.out.println("Nie wymuszam żadnego dziedziczenia !!! Pożyje sobie długo !!!");
                    Thread.sleep(100000);
                    System.out.println("po długim zyciu...kachu, kachu i do piachu ...");
                } catch (InterruptedException ex) {
                    System.out.println("uuppss ...");
                    Logger.getLogger(Watek.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        // Klasa Thread pozwala uruchamiać obiekty implementujące interfejs Runnable
        Thread t= new Thread(runIt);
        t.start();
        System.out.println("Głowny wątek idzie dalej ");
       // wymusimy przerwanie spokojengo snu runIt
        t.interrupt();
    }

    @Override
    public void run() {
        System.out.println("Jestm wątek "+ nazwa +" pożyje sobie "+ czasZycia+ " milisekund!");
        try {
            // metoda pozwala uspić wątek na określony czas , 
            // uśpiony wątek nie zabiera zasobów procesora
            Thread.sleep(czasZycia);
        } catch (InterruptedException ex) {
            Logger.getLogger(Watek.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(nazwa + " ... kachu, kachu i do piachu ...");
        
    }
    
    
    
}
