// deklaracja pakietu
package przyklady;

//sekcja import
import java.util.Date;

// deklaracja klasy
public class Hello {
    
    // statyczna metoda statyczna 
    public static void main(String[] args){
        //deklaracja zmiennej lokalnej
        Date data;
        
        // inicjancja zmiennej lokalnej przy uzyciu konstruktora
        data= new Date();
        
        // wywolanie metody prontln(String arg) na obiekcie System.out  
        System.out.println("Hello , dzisiaj jest: "+data);
        /*  Komentarz
            w wielu 
            liniach  */        
    }
    
}
