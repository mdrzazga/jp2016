package przyklady;

public class WarunkoweAndTernary {

    String greeting;

    public static void main(String args[]) {
        
        // ternary (warunek) ? wyrazenie jesli prwada : wyrazenie jeśli fałsz
        String text = (args.length > 1) ? "Argumenty" : "Brak";
        System.out.println(text);
        
        WarunkoweAndTernary testowy = new WarunkoweAndTernary();
        test(testowy);
    }

    public static void test(WarunkoweAndTernary hello) {
        /* warunkowe AND pierwzyWarunke && drugiWarunek
        Drugi warunke zostanie sprawdzony tylko jeżeli pierwszy jest prawdziwy.
        Pozwala tworzyć ciągi warunków, w których element jest sprawdzany
        tylko jeżeli poprzedni został spełniony.
        Analogicznie isnieje operator warunkowego OR ||,
        w którym drugi warunek jest sprawdzany tylko jeżeli pierwszy jest fałszywy
        */ 
        if (hello != null && hello.greeting != null) {
            System.out.println(hello.greeting);
        }
    }

}
