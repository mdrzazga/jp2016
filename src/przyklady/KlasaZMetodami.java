package przyklady;

import java.io.Serializable;

public class KlasaZMetodami {
    
    public static void main(String args[]){
        int i =0;
        String[] ss = {"a","b","c"};
        for(String s:ss){ i++;}
        if( i%2 ==0 ) i = i -2;
        System.out.println(""+ i);
        // przed użyciem należy utworzyc obiekt klasy
        KlasaZMetodami test = new KlasaZMetodami();
        // do wywołnia metody uzywamy wyrażenia ścieżkowego
        test.metoda();
        int x = 1;
long y = 33;
long z = x*y ;
        
        
        int ret = test.metoda2();
    }
    int i =1;
    //deklaracja metody musi zawierać: zwracany typ, nazwę oraz w nawiasach listę argumentów
    //metoda bez parametrów, która nic nie zwraca (zwraca typ void)
    public void metoda(){
        // ciało metody zawiera sie w nawiasach klamrowych {}
        // bezpośrednio za deklaracją
        System.out.println("Mam dostep do zmiennej instancyjnej i: "+ i);
    }
    
    // metoda zwaracjaca dane w typie int
    public int metoda2(){
        // musi zwracać wartosć odpowiedniego typu
        return i;
    }
    
    // metoda z parametrem
    public long metoda(int j){
        // w ciele metody można odwoływać sie do innych metod z tej samej klasy
        // nie używamy wyrażenia scieżkowego, operujemy na wspólnym obiekcie
        return j + metoda2();
    }
    
}
