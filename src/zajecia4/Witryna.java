
package zajecia4;

/**
 * Zadania 3. Utworzyć klasę Witryna, która pozwoli wypisać na ekranie tablice skupu i sprzedaży
 * walut. Kursy sprzedaży i skupu powinny być różne
 */
public class Witryna {
    private TablicaKursow skup, sprzedaz;

    /**
     * Przkazanie parametrow klasy w konstruktorze
     * @param skup
     * @param sprzedaz 
     */
    public Witryna(TablicaKursow skup, TablicaKursow sprzedaz) {
        this.skup = skup;
        this.sprzedaz = sprzedaz;
    }
    
    /**
     * Wypisuje na ekranie aktualne ceny skupu i sprzedazy
     */
    public void wypisz(){
        System.out.println("Waluta\tskup\tsprzedaż");
        for(String waluta :skup.getWaluty()){
            System.out.format("%s\t%.2f\t%.2f\n", waluta, skup.getKurs(waluta), sprzedaz.getKurs(waluta) );
        }
    }

    public TablicaKursow getSkup() {
        return skup;
    }

    public TablicaKursow getSprzedaz() {
        return sprzedaz;
    }
    
    
    
}
