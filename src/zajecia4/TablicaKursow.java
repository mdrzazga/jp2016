package zajecia4;

/**
 * Zadanie 1. Proszę utworzyć klasę reprezentującą tablice kursów dla zadanych
 * walut: USD, EURO, GBP, CHF, CNY, RUB. Kursy poszczególnych walut można
 * przechowywać w tablicy double []
 *
 */
public class TablicaKursow {

    private String waluty[] = {"USD", "EURO", "GBP", "CHF", "CNY", "RUB"};
    private double kursy[] = {1, 1, 1, 1, 1, 1};

    /**
     * metoda zwracająca tablicę z nazwami walut
     * @return tablice walut
     */
    public String[] getWaluty() {
        return waluty;
    }

    /**
     * metoda zwracająca wartość kursu dla zadanej parametrem waluty 
     * @param waluta
     * @return kurs waluty
     */
    public double getKurs(String waluta) {
        int idx = getIndex(waluta);
        return ( idx >= 0 )?kursy[idx]:-1;
    }
    
    /**
     * 
     * @param waluta
     * @param wartosc 
     */
    public void setKurs(String waluta, double wartosc){
        int idx = getIndex(waluta);
        if(idx >=0) kursy[idx]= wartosc;
    }
    
    /**
     * pomocnicza metoda wyszukuje index w tablicy walut dla zadanego parametru
     * @param waluta
     * @return index waluty w tablicy lub -1 jesli nie znajdzie
     */
    private int getIndex(String waluta) {
        int idx = 0;
        while (idx < waluty.length && !waluty[idx].equals(waluta)) {
            idx++;
        }
        if (idx < waluty.length) {
            return idx;
        } else {
            return -1;
        }
    }
}
