package zajecia4;

public class Main {
    public static void main(String args[]){
        TablicaKursow kursy = new TablicaKursow();
        // sprawdzenie do zadania 1.
        double wartosc = 1;
        for(String waluta: kursy.getWaluty()){
            System.out.print(waluta + ":\t"+ kursy.getKurs(waluta));
            kursy.setKurs(waluta, wartosc ++ );
            System.out.println("\tpo zmianie:\t" + kursy.getKurs(waluta));
        }

        // sprawdzenie do zadania 2
        double testowaWartosc = 10;
        String testowaWaluta = "EURO";
        Kantor kantor = new Kantor();
        kantor.setTablicaKursow(kursy);
        System.out.println("Przeliczam "+testowaWartosc+" "+testowaWaluta + " na PLN " + kantor.przeliczNa(testowaWaluta, testowaWartosc) );
        System.out.println("Przeliczam z "+testowaWartosc+" PLN "+" na "+testowaWaluta+": " + kantor.przeliczZ(testowaWaluta, testowaWartosc) );
        
        // spradzenie do zadania 3
        TablicaKursow skup = new TablicaKursow();
        TablicaKursow sprzedaz = kursy;
        Witryna witryna = new Witryna(skup, sprzedaz);
        witryna.wypisz();
        sprzedaz.setKurs("USD", 3.333333333);
        
        witryna.getSkup().setKurs("USD", 3.111111);
        
        witryna.wypisz();
        
    }
}
