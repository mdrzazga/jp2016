
package zajecia4;

/**
 *Zadanie 2. Utworzyć klasę Kantor
 */
public class Kantor {
    TablicaKursow tablicaKursow;

    /**
     * metoda pozwalająca ustawić tablice kursów utworzoną w zadaniu 1
     * @param tablicaKursow 
     */
    public void setTablicaKursow(TablicaKursow tablicaKursow) {
        this.tablicaKursow = tablicaKursow;
    }
    
    /**
     * metoda przeliczająca wg kursu dla zdanej waluty i wartości na PLN
     * @param waluta 
     * @param wartosc 
     * @return 
     */
    public double przeliczZ(String waluta, double wartosc){
        return tablicaKursow.getKurs(waluta)*wartosc;
    }
    /**
     * metoda przeliczająca na zadaną walutę wartość podaną w PLN
     * @param waluta
     * @param wartosc
     * @return 
     */
    public double przeliczNa(String waluta, double wartosc){
        return tablicaKursow.getKurs(waluta)/wartosc;
    }

    
    
    
}
