# README #

Repozytorium pomocnicze do zajęć Jezyki Programowania gr.13 I Rok/UŚ 

### Co tu znajdziesz ? ###

* Szczątkowe prezentacje wykorzystywane na ćwiczeniach w sekcji [Downloads](https://bitbucket.org/mdrzazga/jp2016/downloads)
* Skan rozdziału angielskiego kursu Java  [JavaObjectsAndClasses](https://bitbucket.org/mdrzazga/jp2016/downloads/JavaObjectsAndClasses.pdf)
* Kod z przykładami w postaci projeku NetBeans w sekcji [Source](https://bitbucket.org/mdrzazga/jp2016/src) 
* Kod można skolonować gitem albo pobrać jako zip w sekcji [Downloads](https://bitbucket.org/mdrzazga/jp2016/downloads)
* Szkielety do zadań ...
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Macie jakiś pomysł ? ###

* Jeżeli chcecie sie czymś podzielić grupą , to miejsce jest dla Was
* Jeżeli chcesz współtworzyć to repozytorium - załuż konto na Bitbucket i daj mi znać !
* [WS-Secutity](https://msdn.microsoft.com/en-us/library/ms735117(v=vs.110).aspx)